import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestPizza {
    @Test
    public void testCheesePizza() {
        IPizza pizza = new Pizza(50, "Standard");
        pizza = new CheeseFilling(pizza);
        assertEquals(60, pizza.getPrice());
        assertEquals("Standard,Cheese", pizza.getComposition());
    }

    @Test
    public void testMeatPizza() {
        IPizza pizza = new Pizza(50, "Standard");
        pizza = new MeatStuffing(pizza);
        assertEquals(70, pizza.getPrice());
        assertEquals("Standard,Meat", pizza.getComposition());
    }
}

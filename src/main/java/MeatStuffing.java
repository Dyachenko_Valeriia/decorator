public class MeatStuffing extends PizzaFilling{ // мясная начинка
    private int priceMeat = 20;
    private String composition = "Meat";

    public MeatStuffing(IPizza pizza) {
        super(pizza.getPrice(), pizza.getComposition(), pizza);
    }

    public int getPrice() {
        return super.getPrice() + priceMeat;
    }

    public String getComposition() {
        return super.getComposition() + "," + composition;
    }
}

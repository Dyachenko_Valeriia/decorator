public interface IPizza {
    public int getPrice();
    public String getComposition();
}

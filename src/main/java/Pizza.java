public class Pizza implements IPizza {
    private int price;
    private String composition;

    public Pizza(int price, String composition) {
        this.price = price;
        this.composition = composition;
    }

    public int getPrice() {
        return this.price;
    }

    public String getComposition() {
        return this.composition;
    }
}

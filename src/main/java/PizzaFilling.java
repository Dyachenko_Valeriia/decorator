public class PizzaFilling implements IPizza { //Начинка
    private int price;
    private String composition;
    private IPizza pizza;

    public PizzaFilling(int price, String composition, IPizza pizza) {
        this.price = price;
        this.composition = composition;
        this.pizza = pizza;
    }

    public int getPrice() {
        return pizza.getPrice();
    }

    public String getComposition() {
        return pizza.getComposition();
    }
}

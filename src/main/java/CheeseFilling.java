public class CheeseFilling extends PizzaFilling { // сырная начинка
    private int priceCheese = 10;
    private String composition = "Cheese";
    public CheeseFilling(IPizza pizza) {
        super(pizza.getPrice(), pizza.getComposition(), pizza);
    }

    public int getPrice() {
        return super.getPrice() + priceCheese;
    }

    public String getComposition() {
        return super.getComposition() + "," + composition;
    }
}
